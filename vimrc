set nocompatible

let g:pathogen_disabled = []

execute pathogen#infect()

colorscheme desert
syntax on
set incsearch
set visualbell
set t_vb=

" let &colorcolumn=join(range(81,999),",")
" highlight ColorColumn ctermbg=darkgray guibg=grey18

set cursorline

set guioptions-=r "remove right scrollbar
set guioptions-=T "remove toobar
set guioptions-=m "remove menu bar

set langmap=ЙQ,ЦW,УE,КR,ЕT,НY,ГU,ШI,ЩO,ЗP,Х{,Ъ},ФA,ЫS,ВD,АF,ПG,РH,ОJ,ЛK,ДL,Ж:,Э\",ЯZ,ЧX,СC,МV,ИB,ТN,ЬM,Б<,Ю>,Ё§,йq,цw,уe,кr,еt,нy,гu,шi,щo,зp,х[,ъ],фa,ыs,вd,аf,пg,рh,оj,лk,дl,э\',яz,чx,сc,мv,иb,тn,ьm,ё§

set scrolloff=5

set tabstop=4
set shiftwidth=4
set hlsearch
set number
set expandtab
set autowriteall

let g:ctrlp_max_files = 0
let g:ctrlp_custom_ignore = {
	\ 'file': '\v(\.cpp|\.cc|\.h|\.java|\.xml|\.gyp|\.gypi)@<!$'
	\ }
let g:ctrlp_clear_cache_on_exit = 0
let g:ctrlp_use_caching = 1
let g:ctrlp_user_command = 'find %s -name "*.gyp*" -or -name "*.java" -or -name "*.cc" -or -name "*.h" -or -name "*.cpp" -or -name "*.rs" -or -name "*.gn*" -or -name "*.xml" -or -name "*.c"'

let g:NERDTreeWinSize=51

map <c-f> :call JsBeautify()<cr>

autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS

" trim whitespaces
autocmd FileType c,cpp,java,javascript autocmd BufWritePre <buffer> :%s/\s\+$//e

